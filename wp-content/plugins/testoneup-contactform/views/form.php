<form role="form" name="testoneup__contactform" method="POST" id="testoneup__contactform" autocomplete="off" >
    <input type="hidden" name="action" value="testoneup_contact_submission" />
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="sr-only"> <?php _e('Name', 'testoneup'); ?> </label>
                <input type="text" name="name" id="name" placeholder="<?php _e('Name', 'testoneup'); ?>" class="form-control" required>
            </div>
            <div class="form-group">
                <label class="sr-only"> <?php _e('Email', 'testoneup'); ?></label>
                <input type="email" name="email" id="email" placeholder="<?php _e('Email', 'testoneup'); ?>" class="form-control" required >
            </div>
            <div class="form-group">
                <label class="sr-only"> <?php _e('Subject', 'testoneup'); ?> </label>
                <input type="text" name="subject" id="subject" placeholder="<?php _e('Subject', 'testoneup'); ?>" class="form-control" required>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="sr-only"> <?php _e('Message', 'testoneup'); ?> </label>
                <textarea name="message" id="message" class="form-control"  placeholder="<?php _e('Message', 'testoneup'); ?>" required></textarea>
            </div>
        </div>
        <?php echo wp_nonce_field('ajax_testoneup_contactform', '_atou_nonce', true, false)?>
    </div>
    <div class="row">
        <div class="col-xs-12 text-center">
            <button type="submit" class="btn btn-primary" data-loading-text="Sending Message..."> Send Message </button>
        </div>
    </div>
</form>
<!-- Modal -->
<div class="modal fade testoneup-thankyou-modal" id="testoneup-thankyou-modal" tabindex="-1" role="dialog" aria-labelledby="testoneup-thankyou-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btn-sm" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <fieldset>
                    <header class="text-center section-header">
                        Thank you!!!
                    </header>
                    <div class=" text-center">
                        <p>
                            We have received your message, we will be in touch with you as soon as possible.
                        </p>
                    </div>
                </fieldset>

            </div>

        </div>
    </div>
</div>