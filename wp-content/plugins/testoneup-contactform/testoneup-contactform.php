<?php
/*
	Plugin Name: Test OneUp Contact Form
	Description: Contact form to save contact in db and send email to admin at the sam time.
	Author: Shankaran Patel
	Version: 1.0
*/

/*
 * Function to handle admin menus
 */
if (!function_exists('testoneup_admin_menu')) {

    function testoneup_admin_menu()
    {
        $hook = add_menu_page('Test OneUp ', 'Test OneUp ', 'administrator', 'testoneup-contact-form', 'testoneup_admin_contacts_manager');
        add_submenu_page('testoneup-contact-form', 'Settings', 'Settings', 'administrator', 'settings', 'testoneup_admin_settings');

        add_action('load-' . $hook, 'testoneup_manager');
    }

}

/*
 * Function to  create new instance of the class TestOneUpContactsManager
*/

if (!function_exists('testoneup_manager')) {
    function testoneup_manager()
    {
        require_once('includes/TestOneUpContactsManager.php');
        global $testOneUpContactsManager;
        $testOneUpContactsManager = new TestOneUpContactsManager();
    }
}
/*
 * Admin listing for contacts
 */
if (!function_exists('testoneup_admin_contacts_manager')) {
    function testoneup_admin_contacts_manager()
    {
        global $testOneUpContactsManager;
        $testOneUpContactsManager->prepare_items();
        require_once('includes/TestOneUpContactsManager.php');
        ?>
        <div class="wrap tesoneup_contacts">
            <h1>Test OneUp Contacts</h1>

            <form id="posts-filter" method="get">
                <input type="hidden" name="page" value="testoneup_manager"/>
                <?php $testOneUpContactsManager->display();
                wp_nonce_field(plugin_basename(__FILE__), 'testoneup_manager_nonce_name'); ?>
            </form>
        </div>
        <?php
    }
}

/*
 * Function
 */
if (!function_exists('testoneup_admin_settings')) {
    function testoneup_admin_settings()
    {
        include('views/admin/settings.php');
    }
}
/*
 * Wordpress hook testoneup_admin_menu to admin_menu
 */
add_action('admin_menu', 'testoneup_admin_menu');


/*
 * Plugin activation function
 * Write plugin settings and create neccessary tables for plugin in database
 */
if (!function_exists('testoneup_activation')) {
    function testoneup_activation()
    {
        testoneup_settings();
        testoneup_create_table();
    }
}


/*
* Function to register default settings of plugin
*/
if (!function_exists('testoneup_settings')) {
    function testoneup_settings()
    {
        add_option('testOneUp_On_Subject', "New TestOneUp Contact");
    }
}


/*
* Function to create a new tables in database
*/
if (!function_exists('testoneup_create_table')) {
    function testoneup_create_table()
    {
        global $wpdb;
        $testOneUpContact_table = $wpdb->prefix . "tOneUpCF";
        add_option('testOneUp_ContactTable', $testOneUpContact_table);

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        $sql = "
			CREATE TABLE IF NOT EXISTS `" . $testOneUpContact_table . "` (
			  `id` int(11) NOT NULL auto_increment,
			  `name` varchar(120) NOT NULL,
			  `email` varchar(120) NOT NULL,
			  `message` TEXT NOT NULL,
			  `subject` varchar(255) NOT NULL,
			  `ip` varchar(50) NOT NULL,
			  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			  PRIMARY KEY  (`id`) )
			  ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        dbDelta($sql);
    }
}

/*
 * Register testoneup_activation to be called on this plugin activation
 */
register_activation_hook(__FILE__, 'testoneup_activation');

if (!function_exists('testoneup_deactivation')) {
    function testoneup_deactivation()
    {
        // No action required for now
    }
}
register_deactivation_hook(__FILE__, 'testoneup_deactivation');

/*
 * Test OneUp Contact form
 */
function testoneup_contactform()
{
    include_once('views/form.php');
}

/**
 * Test OneUp Short Code
 * @return string
 */
function testoneup_shortcode()
{
    ob_start();
    testoneup_contactform();
    return ob_get_clean();
}

/*
 * Register shortcode as testoneup_contacform
 */
add_shortcode('testoneup_contactform', 'testoneup_shortcode');


function testoneup_send_contact_submission_email()
{
    global $testOneUpContactData;

    $name = $testOneUpContactData['name'];
    $email = $testOneUpContactData['email'];
    $subject = $testOneUpContactData['subject'];
    $message = $testOneUpContactData['message'];

    // get the blog administrator's email address
    $to = get_option('admin_email');

    $headers = "From: $name <$email>" . "\r\n";

    // If email has been process for sending, display a success message
    if (wp_mail($to, $subject, $message, $headers)) {
        return true;
    }

    return false;
}

function testoneup_contact_submission_capture()
{
    global $wpdb;
    global $testOneUpContactData;

    $testOneUp_ContactTable = get_option('testOneUp_ContactTable');

    $wpdb->insert($testOneUp_ContactTable, array(
            'name' => $testOneUpContactData['name'],
            'email' => $testOneUpContactData['email'],
            'message' => $testOneUpContactData['message'],
            'subject' => $testOneUpContactData['subject'],
            'ip' => $_SERVER['REMOTE_ADDR'],
        )
    );

    $contactId = $wpdb->insert_id;

    return $contactId;
}

/*
 * Contact form ajax submission
 */
function process_testoneup_contact_submission()
{
    global $wpdb;
    global $testOneUpContactData;
    $testOneUpContactData = $_POST;

    $testOneUpContactData['name'] = sanitize_text_field($testOneUpContactData['name']);
    $testOneUpContactData['email'] = sanitize_email($testOneUpContactData['email']);
    $testOneUpContactData['subject'] = sanitize_text_field($testOneUpContactData['subject']);
    $testOneUpContactData['message'] = esc_textarea($testOneUpContactData['message']);

    testoneup_contact_submission_capture();

    if (!$wpdb->last_error) {
        testoneup_send_contact_submission_email();
        $response = array('success' => 'true');
    } else {
        $response = array('success' => 'false');
    }

    wp_send_json($response);
}

function testoneup_contactform_scripts()
{
    wp_enqueue_script('testoneup-contactform-script', plugin_dir_url(__FILE__) . 'js/script.js', array('jquery'));
    wp_localize_script('testoneup-contactform-script', 'ajax_object_testoneup',
        array(
            'ajax_url' => admin_url('admin-ajax.php'),
            'plugin_base_path' => plugin_dir_url(__FILE__)
        )
    );

}

add_action('wp_enqueue_scripts', 'testoneup_contactform_scripts');

add_action('wp_ajax_testoneup_contact_submission', 'process_testoneup_contact_submission');
add_action('wp_ajax_nopriv_testoneup_contact_submission', 'process_testoneup_contact_submission');
