jQuery(document).ready(function ($) {
    var form = $('form[name="testoneup__contactform"]'),
        submitButton = form.find('button[type="submit"]');

    form.off('submit.testoneup').on('submit.testoneup', function (e) {
        e.preventDefault();
        submitButton.button('loading');
        $.ajax({
            type: 'POST',
            url: ajax_object_testoneup.ajax_url,
            data: form.serialize(),
            dataType: 'json',
            success: function (response) {
                $(form)[0].reset();
                submitButton.button('reset');

                $('.testoneup-thankyou-modal').modal({
                    show: true,
                    backdrop: 'static'
                });
            },
            error: function () {
                submitButton.button('reset');
            }
        });
    });

});