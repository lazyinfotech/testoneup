<?php

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class TestOneUpContactsManager extends WP_List_Table
{

    /** Class constructor */
    public function __construct()
    {

        parent::__construct([
            'singular' => __('Contact', 'testoneup'), //singular name of the listed records
            'plural' => __('Contacts', 'testoneup'), //plural name of the listed records
            'ajax' => false //does this table support ajax?
        ]);


    }


    /**
     * Retrieve contacts data from the database
     *
     * @param int $per_page
     * @param int $page_number
     *
     * @return mixed
     */
    public function get_contacts($per_page = 5, $page_number = 1)
    {

        global $wpdb;

        $testOneUp_ContactTable = get_option('testOneUp_ContactTable');

        $sql = "SELECT * FROM {$testOneUp_ContactTable}";

        if (!empty($_REQUEST['orderby'])) {
            $sql .= ' ORDER BY ' . esc_sql($_REQUEST['orderby']);
            $sql .= !empty($_REQUEST['order']) ? ' ' . esc_sql($_REQUEST['order']) : ' ASC';
        }

        $sql .= " LIMIT $per_page";
        $sql .= ' OFFSET ' . ($page_number - 1) * $per_page;

        $result = $wpdb->get_results($sql, 'ARRAY_A');

        return $result;
    }


    /**
     * Delete a contact record.
     *
     * @param int $id contact ID
     */
    public static function delete_contact($id)
    {
        global $wpdb;

        $testOneUp_ContactTable = get_option('testOneUp_ContactTable');

        $wpdb->delete(
            "{$testOneUp_ContactTable}",
            ['ID' => $id],
            ['%d']
        );
    }


    /**
     * Returns the count of records in the database.
     *
     * @return null|string
     */
    public static function record_count()
    {
        global $wpdb;

        $testOneUp_ContactTable = get_option('testOneUp_ContactTable');

        $sql = "SELECT COUNT(*) FROM {$testOneUp_ContactTable}";

        return $wpdb->get_var($sql);
    }


    /** Text displayed when no contact data is available */
    public function no_items()
    {
        _e('No contacts avaliable.', 'sp');
    }


    /**
     * Render a column when no column specific method exist.
     *
     * @param array $item
     * @param string $column_name
     *
     * @return mixed
     */
    public function column_default($item, $column_name)
    {
        switch ($column_name) {
            case 'id':
            case 'name':
            case 'email':
            case 'message':
            case 'ip':
            case 'date':
                return $item[$column_name];
            default:
                return print_r($item, true);
        }

    }


    /**
     *  Associative array of columns
     *
     * @return array
     */
    function get_columns()
    {
        $columns = [
            'name' => __('Name', 'testoneup'),
            'email' => __('Email', 'testoneup'),
            'message' => __('Message', 'testoneup'),
            'ip' => __('IP Address', 'testoneup'),
            'date' => __('Date', 'testoneup')
        ];

        return $columns;
    }


    /**
     * Columns to make sortable.
     *
     * @return array
     */
    public function get_sortable_columns()
    {
        $sortable_columns = array(
            'name' => array('name', true),
            'email' => array('email', false)
        );

        return $sortable_columns;
    }

    /**
     * Handles data query and filter, sorting, and pagination.
     */
    public function prepare_items()
    {

        $this->_column_headers = $this->get_column_info();

        $per_page = $this->get_items_per_page('contacts_per_page', 5);
        $current_page = $this->get_pagenum();
        $total_items = self::record_count();

        $this->set_pagination_args([
            'total_items' => $total_items, //WE have to calculate the total number of items
            'per_page' => $per_page //WE have to determine how many items to show on a page
        ]);

        $this->items = self::get_contacts($per_page, $current_page);
    }


}
