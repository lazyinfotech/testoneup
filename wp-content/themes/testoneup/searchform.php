<!-- search -->
<form class="search-form form-inline" method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="search-input form-control" type="search" name="s" placeholder="<?php _e( 'To search, type and hit enter.', 'oneup' ); ?>">
	<button class="search-submit btn btn-default" type="submit" role="button"><?php _e( 'Search', 'oneup' ); ?></button>
</form>
<!-- /search -->
