<?php
/*
 *  Author: Shankaran Patel
 *  OneUp Theme
 */

function oneup_setup()
{

    global $content_width;

    if (!isset($content_width)) {
        $content_width = 900;
    }

    if (function_exists('add_theme_support')) {
        // Add Menu Support
        add_theme_support('menus');

        // Add Thumbnail Theme Support
        add_theme_support('post-thumbnails');
        add_image_size('large', 700, '', true); // Large Thumbnail
        add_image_size('medium', 250, '', true); // Medium Thumbnail
        add_image_size('small', 120, '', true); // Small Thumbnail
        add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

        // Enables post and comment RSS feed links to head
        add_theme_support('automatic-feed-links');

        // Add Support for Custom Header - Uncomment below if you're going to use
        add_theme_support('custom-header', array(
            'default-image' => '',
            'header-text' => false,
            'default-text-color' => '000',
            'width' => 0,
            'height' => 0,
            'random-default' => false,
            'wp-head-callback' => '',
            'admin-head-callback' => '',
            'admin-preview-callback' => ''
        ));

        // Localisation Support
        load_theme_textdomain('oneup', get_template_directory() . '/languages');
    }
}

add_action('after_setup_theme', 'oneup_setup');

// Load Test OneUp scripts (header.php)
function oneup_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_register_script('bootstrap', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js', array('jquery'), '3.3.6'); // Bootstrap
        wp_enqueue_script('bootstrap'); // Enqueue it!

        wp_register_script('oneupscript', get_template_directory_uri() . '/js/app.js', array('jquery'), '1.0.0'); // App scripts
        wp_enqueue_script('oneupscript'); // Enqueue it!

    }
}

// Add Actions
add_action('init', 'oneup_header_scripts'); // Add Custom Scripts to wp_head

// Load Test OneUp styles
function oneup_styles()
{
    wp_register_style('bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css', array(), '3.3.6', 'all');
    wp_enqueue_style('bootstrap'); // Enqueue it!

    wp_register_style('font-awesome', get_template_directory_uri() . '/assets/font-awesome/css/font-awesome.min.css', '4.5.0', 'all');
    wp_enqueue_style('font-awesome'); // Enqueue it!

    wp_register_style('font-awesome', get_template_directory_uri() . '/style.css', 'v1', 'all');
    wp_enqueue_style('font-awesome'); // Enqueue it!

    wp_enqueue_style( 'google-fonts-raleway', 'https://fonts.googleapis.com/css?family=Raleway:400,600,500', false );

    wp_enqueue_style( 'google-fonts-opensans', 'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700', false );

    // Load our main stylesheet.
    wp_enqueue_style('oneup-style', get_stylesheet_uri());
}

add_action('wp_enqueue_scripts', 'oneup_styles'); // Add Theme Stylesheet


function oneup_wp_page_menu()
{
    echo '<ul class="nav navbar-nav navbar-right responsive-nav main-nav-list">';
    wp_list_pages(array('title_li' => '', 'depth' => 1));
    echo '</ul>';
}

// Register Test OneUp Navigation
function register_oneup_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'oneup'), // Main Navigation
        'footer-menu' => __('Footer Menu', 'oneup') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

add_action('init', 'register_oneup_menu'); // Add Test OneUp Menu

// Add Slug to Body class for better css control from pages to pages
function oneup_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

add_filter('body_class', 'oneup_body_class');
