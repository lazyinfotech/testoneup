<?php get_header(); ?>
	<div class="container">
		<main role="main">
			<!-- section -->
			<section>

				<!-- article -->
				<article id="post-404">

					<h1><?php _e( 'Page not found', 'oneup' ); ?></h1>
					<h2>
						<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'oneup' ); ?></a>
					</h2>

				</article>
				<!-- /article -->

			</section>
			<!-- /section -->
		</main>
	</div>
<?php get_sidebar(); ?>

<?php get_footer(); ?>
