<?php get_header(); ?>

<?php if (get_option('show_on_front') == 'page') : ?>
    <?php include(get_page_template()); ?>
<?php else :?>
    <?php if(get_header_image()) : ?>
        <div class="header__image">
            <img src="<?php header_image(); ?>" alt="" />
        </div>
    <?php endif; ?>

    <div class="container">
        <section class="contact-form-section" id="contact" >
            <header class="text-center section-header">CONTACT US</header>
            <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

            <div class="contact-form-wrapper">
                <?php echo do_shortcode('[testoneup_contactform]'); ?>
            </div>

        </section>
    </div>
<?php endif; ?>

<?php get_footer(); ?>