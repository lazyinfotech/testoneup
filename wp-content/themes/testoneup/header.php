<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title(''); ?><?php if (wp_title('', false)) {
            echo ' :';
        } ?><?php bloginfo('name'); ?></title>

    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.png">
    <link rel="apple-touch-icon-precomposed"
          href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-touch-icon.png"/>
    <meta name="msapplication-TileImage"
          content="<?php echo get_template_directory_uri(); ?>/img/icons/msapplication-tile-image.png"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php bloginfo('description'); ?>">

    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
    <header class="Header ">
        <nav class="navbar navbar-default navbar-inverse " role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle " data-toggle="collapse" data-target=".header-menu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a href="<?php echo home_url(); ?>" class="navbar-brand brand-logo logo-small">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Logo" class="logo-img">
                    </a>

                </div>
                <div class="navbar-collapse collapse header-menu" >
                    <?php
                    wp_nav_menu(
                        array('theme_location' => 'header-menu',
                            'container' => false,
                            'menu_class' => 'nav navbar-nav navbar-right responsive-nav main-nav-list',
                            'fallback_cb' => 'oneup_wp_page_menu'
                        )
                    );
                    ?>
                </div>
            </div>
        </nav>
    </header>

    <div class="Body Application">
