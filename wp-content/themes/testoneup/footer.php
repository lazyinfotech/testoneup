
    </div>
    <!-- footer -->

    <footer class="Footer" role="contentinfo">
        <nav class="navbar navbar-inverse ">
            <div class="container">
                <ul class="nav navbar-nav  footer-menu footer-social-nav">
                    <li>
                        <a> <i class="fa fa-facebook"></i> </a>
                    </li>
                    <li>
                        <a> <i class="fa fa-twitter"></i> </a>
                    </li>
                    <li>
                        <a> <i class="fa fa-google-plus"></i> </a>
                    </li>
                    <li>
                        <a> <i class="fa fa-pinterest"></i> </a>
                    </li>
                    <li>
                        <a> <i class="fa fa-instagram"></i> </a>
                    </li>
                    <li>
                        <a> <i class="fa fa-stumbleupon"></i> </a>
                    </li>
                    <li>
                        <a> <i class="fa fa-rss"></i> </a>
                    </li>
                </ul>

            </div>
        </nav>
        <div class="copyright">
            © 2015 Axure Themes
        </div>

    </footer>
    <!-- /footer -->

    <?php wp_footer(); ?>

	</body>
</html>
